const { app, BrowserWindow, ipcMain, dialog } = require('electron');
const puppeteer = require('puppeteer');
const fs = require('fs');
const path = require('path');
let win;

function createWindow() {
  win = new BrowserWindow({
    width: 1200,
    height: 750,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
      enableRemoteModule: true,
      preload: path.join(__dirname, 'preload.js'),
    }
  });

  win.loadFile('src/index.html');
  // win.webContents.openDevTools();

  const { globalShortcut } = require('electron');
  globalShortcut.register('CommandOrControl+Shift+I', () => {
    win.webContents.openDevTools();
  });
}

app.whenReady().then(createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

ipcMain.on('take-screenshot', async (event, urls, time = 1, width = 1920, customCSS, saveFolderPath) => {
  try {

    if (!fs.existsSync(saveFolderPath)) {
      event.sender.send('log', `Save folder does not exist. Creating folder at: ${saveFolderPath}`);
      fs.mkdirSync(saveFolderPath, { recursive: true });
    }
    const browser = await puppeteer.launch({ headless: true });
    const page = await browser.newPage();
    await page.setViewport({ width: parseInt(width, 10), height: 1080 });

    for (let index = 0; index < urls.length; index++) {
      try {
        event.sender.send('log', (`Taking ${index + 1}:  ${urls[index]}`));
        await page.goto(urls[index], { waitUntil: 'networkidle2' });
        await page.waitForTimeout(time * 1000);

        if (customCSS) {
          await page.addStyleTag({ content: customCSS });
        }

        const datetime = new Date().toLocaleString();
        await page.evaluate((datetime) => {
          const div = document.createElement('div');
          div.style.position = 'fixed';
          div.style.top = '2px';
          div.style.right = '2px';
          div.style.backgroundColor = 'rgba(0, 0, 0, 0.7)';
          div.style.color = 'white';
          div.style.padding = '3px';
          div.style.zIndex = '1000';
          div.textContent = datetime;
          document.body.appendChild(div);
        }, datetime);

        const screenshot = await page.screenshot({ fullPage: true });

        const fileName = `screenshot-${Date.now()}.png`;
        const filePath = path.join(saveFolderPath, fileName);
        try {
          fs.writeFileSync(filePath, screenshot);
          event.sender.send('screenshot-captured', filePath, urls[index], `${index + 1}/${urls.length}`);
          event.sender.send('log', (`Saved ${index + 1}: ${filePath}`));
          if (index + 1 == urls.length)
          event.sender.send('log', (`Done !!!`));
        } catch (error) {
          event.sender.send('log', 'Failed to save screenshot:', error);
        }
      } catch (error) {
        event.sender.send('log', `Failed to capture screenshot for ${urls[index]}:`, error);
      }
    }

    await browser.close();
  } catch (error) {
    event.sender.send('log', 'Failed to take screenshots:', error);
  }
});

ipcMain.on('export-data', async (event, data) => {
  const filePath = dialog.showSaveDialogSync({
    title: 'Save JSON',
    defaultPath: path.join(app.getPath('desktop'), 'urls_screenshot_data.json'),
    filters: [{ name: 'JSON Files', extensions: ['json'] }]
  });

  if (filePath) {
    const jsonData = JSON.stringify(data, null, 2);
    fs.writeFile(filePath, jsonData, (err) => {
      if (err) {
        event.sender.send('error', `Failed to save JSON: ${err}`);
      } else {
        event.sender.send('log', `JSON saved to: ${filePath}`);
      }
    });
  }
});

ipcMain.on('reset', () => {
  win.reload();
});

ipcMain.on('choose-save-folder', async (event) => {
  const result = await dialog.showOpenDialog(win, {
    title: 'Choose Folder to Save Screenshots',
    properties: ['openDirectory'],
  });

  if (!result.canceled && result.filePaths.length > 0) {
    const folderPath = result.filePaths[0];
    savePath = folderPath; // Store the chosen folder path
    event.sender.send('save-folder-chosen', folderPath);
    event.sender.send('log', `Folder path: ${folderPath}`);
  }
});

try {
  require('electron-reloader')(module);
} catch { }
