const { ipcRenderer, shell, remote } = require('electron'); // Include remote
const fs = require('fs');
const path = require('path');

const jsonFile = document.getElementById('jsonFile');
const urlsInput = document.getElementById('urlsInput');
const time = document.getElementById('delayInput');
const widthInput = document.getElementById('widthInput');
const cssInput = document.getElementById('cssInput');
const saveFolderInput = document.getElementById('saveFolderInput');
const saveFolderPathInput = document.getElementById('saveFolderPathInput'); // New label to show save folder path

const screenshotButton = document.getElementById('screenshotButton');
const resetButton = document.getElementById('reset');
const openFolderButton = document.getElementById('openFolderButton');
const exportButton = document.getElementById('exportButton');
const window_content = document.getElementById('window_content');
const blink = document.getElementById('blink');

let lastScreenshotPath = '';
let saveFolderPath = '';

saveFolderInput.addEventListener('click', () => {
  ipcRenderer.send('choose-save-folder');
});

screenshotButton.addEventListener('click', () => {
  if (!saveFolderPath) {
    alert('Please choose a folder to save screenshots first.');
    return;
  }
  screenshotContainer.innerHTML = '';
  const urls = urlsInput.value.split(',').map(url => url.trim());
  const customCSS = cssInput.value;
  ipcRenderer.send('take-screenshot', urls, time.value, widthInput.value, customCSS, saveFolderPath);

});

ipcRenderer.on('screenshot-captured', (event, imgData, url, text) => {
  const imgElement = document.createElement('img');
  imgElement.src = imgData;
  screenshotContainer.appendChild(imgElement);
  lastScreenshotPath = imgData;
  openFolderButton.style.display = 'block';
});

ipcRenderer.on('save-folder-chosen', (event, folderPath) => {
  saveFolderPath = folderPath;
  // saveFolderPathInput.textContent = `Save folder: ${folderPath}`; // Display the chosen folder path
  saveFolderPathInput.value = folderPath;
  appendLog(`Save folder chosen: ${folderPath}`);
});

saveFolderPathInput.addEventListener('input', () => {
  saveFolderPath = saveFolderPathInput.value;
});

jsonFile.addEventListener('change', (event) => {
  const file = event.target.files[0];
  if (!file) {
    return;
  }
  const reader = new FileReader();
  reader.onload = (e) => {
    try {
      const content = e.target.result;
      const obj = JSON.parse(content);

      urlsInput.value = obj.urls.join(",\n");
      time.value = obj.delay;
      widthInput.value = obj.width ?? 1660;
      cssInput.value = obj.css;
      saveFolderPathInput.value = obj.saveFolder ?? '';
      saveFolderPath = obj.saveFolder ?? ''
      appendLog('JSON file loaded successfully');
    } catch (err) {
      console.log('jsonFile.addEventListener err:', err);
      appendLog('Failed to parse JSON file', err);
    }
  };
  reader.readAsText(file);
});

resetButton.addEventListener('click', () => {
  screenshotContainer.innerHTML = '';
  urlsInput.value = '';
  time.value = '0';
  widthInput.value = '1660';
  cssInput.value = '';
  openFolderButton.style.display = 'none';
  window_content.innerHTML = '';
  saveFolderPath = '';
  saveFolderPathInput.textContent = ''; // Clear the save folder path label

  window_content.appendChild(blink);

  ipcRenderer.send('reset');
});

openFolderButton.addEventListener('click', () => {
  if (lastScreenshotPath) {
    const screenshotDir = path.dirname(lastScreenshotPath);
    shell.openPath(screenshotDir);
  }
});

exportButton.addEventListener('click', () => {
  const data = {
    urls: urlsInput.value.split(',').map(url => url.trim()),
    delay: time.value,
    width: widthInput.value,
    css: cssInput.value,
    saveFolder: saveFolderPathInput.value,
  };

  ipcRenderer.send('export-data', data);
});

ipcRenderer.on('data-exported', (event, filePath) => {
  shell.showItemInFolder(filePath);
});

ipcRenderer.on('log', (event, mess, obj) => {
  appendLog(mess, obj);
});

ipcRenderer.on('error', (event, mess, obj) => {
  const errorMess = `Error: ${mess}`;
  appendLog(errorMess, obj);
});

const appendLog = (mess, obj) => {
  console.log(mess, obj);
  const br = document.createElement('br');
  const span = document.createElement('span');
  const textNode = document.createTextNode(mess);
  span.appendChild(textNode);
  window_content.appendChild(br);
  window_content.appendChild(span);
  window_content.appendChild(blink);
};
